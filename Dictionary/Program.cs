﻿/*Домашнее задание

Реализуем свой собственный словарь

Реализуйте класс OtusDictionary, который позволяет оперировать int-овыми значениями в качестве ключей и строками в качестве значений. 
Для добавления используйте метод void Add(int key, string value), а для получения элементов - string Get(int key). 
Внутреннее хранилище реализуйте через массив. При нахождении коллизий, создавайте новый массив в два раза больше и не забывайте 
пересчитать хеши для всех уже вставленных элементов. Метод GetHashCode использовать не нужно и массив/список объектов 
по одному адресу создавать тоже не нужно (только один объект по одному индексу). Словарь не должен давать сохранить null в качестве 
строки, соответственно, проверять заполнен элемент или нет можно сравнивая строку с null.

Описание/Пошаговая инструкция выполнения домашнего задания:

    Реализуйте метод Add с неизменяемым массивом размером 32 элемента (выбрасывать исключение, если уже занято место).
    Реализуйте метод Get.
    Реализуйте увеличение массива в два раза при нахождении коллизий.
    Убедитесь, что класс работает без ошибок (например, Get несуществующего элемента не бросает исключений), помимо заданных вами. Если это не так, то доработайте.
    Добавьте к классу возможность работы с индексатором.
*/

namespace Dictionary;

internal class Program
{
    static void Main()
    {
        MyDictionary myDict = new();

        Console.WriteLine("--- Add Values ---");
        AddElement(myDict, 0, "zero");
        AddElement(myDict, 11, "eleven");
        AddElement(myDict, 22, "twenty two");
        AddElement(myDict, 22, "another twenty two");
        AddElement(myDict, 33, "thirty three");
        AddElement(myDict, 43, "fourty three");
        AddElement(myDict, 200, null);
        AddElement(myDict, -100, "minus hundred");

        Console.WriteLine("\n--- Get Values ---");
        Console.WriteLine($"Count is {myDict.Count}.");
        GetElement(myDict, 0);
        GetElement(myDict, 11);
        GetElement(myDict, 22);
        GetElement(myDict, 33);
        GetElement(myDict, 43);
        GetElement(myDict, 15);
        GetElement(myDict, -100);

        Console.WriteLine("\n--- Use Indexer ---");
        Console.Write("Adding and reading via indexer [5] = five ... ");
        myDict[5] = "five";
        if (myDict[5] == "five")
        {
            Console.WriteLine("success");
        }
        else
        {
            Console.WriteLine("failed");
        }
    }

    private static void AddElement(MyDictionary myDict, int key, string? value)
    {
        Console.Write($"Adding [{key}] = {value ?? "*null*"} ... ");
        try
        {
            if (myDict.Add(key, value!))
            {
                Console.WriteLine("success");
            }
            else
            {
                Console.WriteLine("failed");
            }
        }
        catch (ArgumentException exc)
        {
            Console.WriteLine("exception: " + exc.Message);
        }
        catch (OutOfMemoryException exc)
        {
            Console.WriteLine("exception: " + exc.Message);
            throw;
        }
    }

    private static void GetElement(MyDictionary myDict, int key)
    {
        try
        {
            Console.Write($"[{key}] = ");
            Console.WriteLine(myDict.Get(key));
        }
        catch (KeyNotFoundException)
        {
            Console.WriteLine("key not found exception");
        }
        
    }
}