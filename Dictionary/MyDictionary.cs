﻿/*Домашнее задание

Реализуем свой собственный словарь

Реализуйте класс OtusDictionary, который позволяет оперировать int-овыми значениями в качестве ключей и строками в качестве значений. 
Для добавления используйте метод void Add(int key, string value), а для получения элементов - string Get(int key). 
Внутреннее хранилище реализуйте через массив. При нахождении коллизий, создавайте новый массив в два раза больше и не забывайте 
пересчитать хеши для всех уже вставленных элементов. Метод GetHashCode использовать не нужно и массив/список объектов 
по одному адресу создавать тоже не нужно (только один объект по одному индексу). Словарь не должен давать сохранить null в качестве 
строки, соответственно, проверять заполнен элемент или нет можно сравнивая строку с null.

Описание/Пошаговая инструкция выполнения домашнего задания:

    Реализуйте метод Add с неизменяемым массивом размером 32 элемента (выбрасывать исключение, если уже занято место).
    Реализуйте метод Get.
    Реализуйте увеличение массива в два раза при нахождении коллизий.
    Убедитесь, что класс работает без ошибок (например, Get несуществующего элемента не бросает исключений), помимо заданных вами. Если это не так, то доработайте.
    Добавьте к классу возможность работы с индексатором.
*/

namespace Dictionary;

internal class MyDictionary
{
    private MyKvp[] _contents;
    public int Capacity { get; private set; }
    public int Count { get; private set; }

    public MyDictionary()
    {
        Capacity = 32;
        _contents = new MyKvp[Capacity];
    }

    private class MyKvp
    {
        public int Key { get; set; }
        public string Value { get; set; }

        public MyKvp(int key, string value)
        {
            Key = key;
            Value = value;
        }
    }

    public bool Add(int key, string value)
    {
        if (value is null) 
        { 
            throw new ArgumentNullException(nameof(value));
        }

        do
        {
            int index = Math.Abs(key % Capacity);
            if (_contents[index] is null)
            {
                // это новый элемент, добавляем
                _contents[index] = new MyKvp(key, value);
                Count++;
                return true;
            }
            if (_contents[index].Key == key)
            {
                throw new ArgumentException("Элемент с таким ключем уже есть в словаре.");
            }
            // коллизия. Увеличиваем размер массива
            if (!IncreaseCapacity())
            {
                throw new OutOfMemoryException("Объем словаря превысил максимум для этой коллекции.");
            }
        } 
        while (true);
    }

    public string Get(int key)
    {
        int index = Math.Abs(key % Capacity);
        if (_contents[index] is null) 
        {
            // элемента нет в массиве
            throw new KeyNotFoundException();
        }
        if (_contents[index].Key == key)
        {
            // элемент с нужным ключом найден
            return _contents[index].Value;
        }
        // элемент в массиве есть, но ключ не совпал
        throw new KeyNotFoundException();
    }

    private bool IncreaseCapacity()
    {
        int newCapacity;
        if (Capacity == int.MaxValue)
        {
            return false;
        }
        if (Capacity > int.MaxValue / 2)
        {
            newCapacity = int.MaxValue;
        }
        else
        {
            newCapacity = Capacity * 2;
        }

#if DEBUG
        Console.Write($" ** Debug: Increasing capacity from {Capacity} to {newCapacity} ** ");
#endif

        MyKvp[] oldContents = _contents;
        _contents = new MyKvp[newCapacity];

        for (int i = 0; i < Capacity; i++)
        {
            if (oldContents[i] is not null)
            {
                int index = Math.Abs(oldContents[i].Key % newCapacity);
                _contents[index] = oldContents[i];
            }
        }
        Capacity = newCapacity;
        return true;
    }

    // Индексатор
    public string? this[int key]
    {
        get => Get(key);
        // не смог грамотно обработать это предупреждение. Возвращаемый тип является так же и входным для сеттера.
        set => _ = Add(key, value);
    }
}
